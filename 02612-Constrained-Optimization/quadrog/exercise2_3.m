tic
load('QP_Test.mat')


% 1/2 * x*H*x + g*x
% l <= x <= u
% -C *x <= -dl
% C *x <= du

% x = quadprog(H,f,A,b,Aeq,beq,lb,ub)



A = [-C;C];
B = [-dl;du];
[U,fval1,~,output] = quadprog(H,g,A,B,[],[],l,u);

% C *x -dl >=0
% -C *x + du >= 0
% x -l >= 0
% -x + u >= 0
A = [C;-C;eye(size(C));-1*eye(size(C))];
B = [-dl;du;-l;u];
[U,fval2,~,output] = quadprog(H,g,A,B);

disp("With box constraints")
disp(fval1)
disp("Without box constraints")
disp(fval1)

% final function value is -4.8537e+04
toc
