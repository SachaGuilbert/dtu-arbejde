function x = qp_interior_point_ineq(H, g, A, b, C, d, x0)
    % Input:
    % H - Quadratic term in the objective function (n x n matrix)
    % g - Linear term in the objective function (n x 1 vector)
    % A - Equality constraint matrix (n x m_eq matrix)
    % b - Equality constraint vector (m_eq x 1 vector)
    % C - Inequality constraint matrix (n x m_ineg matrix)
    % d - Inequality constraint vector (m x 1 vector)
    % x0 - Initial guess (n x 1 vector)
    
    % Output:
    % x - Optimal solution
    % fval - Objective function value at optimal solution
    % exitflag - Exit condition
    % iter - Number of iterations
    
    % Tolerance and maximum iterations
    tol = 1e-8;
    max_iter = 10000;
    
    % Problem size
    n = length(g)
    m_eq = size(A, 2)
    m_ineq = size(C, 2)
    
    % Initial point
    x = x0;
    s = ones(m_ineq, 1);
    z = ones(m_ineq, 1);
    y = zeros(m_eq, 1);
    %duality measure:
    alpha = 0.005;
    for i = 1:max_iter,
        mu = s'*z / m_ineq
        if mu < tol,
            break;
        endif
        KKT = [H, -A, -C, zeros(n, m_ineq);
            -A', zeros(m_eq, m_eq + m_ineq + m_ineq);
            -C', zeros(m_ineq, m_eq + m_ineq), eye(m_ineq);
            zeros(m_ineq, n + m_eq), diag(s), diag(z)];
            
        %residuals
        res_L = H*x + g - A*y - C*z;
        res_A = b - A'*x;
        res_C = d - C'*x + s;
        res_SZ = diag(s) * diag(z) * ones(m_ineq, 1);
        
        %solve
        sigma = 0.5;
        direction = KKT \ [-res_L; -res_A; -res_C; -res_SZ  + sigma * mu];
        new_xyzs = [x; y; z; s] + alpha * direction;
        x = new_xyzs(1:n);
        y = new_xyzs(n+1:n+m_eq);
        z = new_xyzs(n+m_eq+1:end - m_ineq);
        s = new_xyzs(end-m_ineq+1:end);
    endfor
endfunction
