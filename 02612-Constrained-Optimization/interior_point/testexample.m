% Small test example
H = [2 0; 0 2];
g = [-2; -5];
A = zeros(n, 0);
b = [];
C = diag([1; 1]);
d = [10; 10];
x0 = [0.5; 0.5]; % Initial guess

[X, OBJ, INFO, LAMBDA] = qp (x0, H, g, A, b, [], [], d, C, [])
qp_interior_point_ng(H, g, A, b, C, d, x0);



load QP_Test.mat
x0 = zeros(200,1);

[X, OBJ, INFO, LAMBDA] = qp (x0, H, g, [], [], l, u, dl, C, du);
X


C_all = zeros(200, 800);
C_all(:, 1:200) = C;
C_all(:, 201:400) = -C;
C_all(:, 401:600) = eye(200);
C_all(:, 601:800) = -eye(200);
d_all = [dl; -du; l; -u];
A = zeros(length(x0), 0);

x = qp_interior_point_ng(H, g, A, [], C_all, d_all, x0);
