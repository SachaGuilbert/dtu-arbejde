clear all
close all
tic

load('QP_Test.mat')

% 1/2 * x*H*x + g*x
% l <= x <= u
% dl <= C *x <= du

% C *x -dl >=0
% -C *x + du >= 0
% x -l >= 0
% -x + u >= 0

A = [C;-C;eye(size(C));-1*eye(size(C))];
b = [-l;u;-dl;du];
%
% % qpsolverActiveSet(H,g,A,b,ones(200,1))
% % Example usage:
%
x0 = gotofeasablepoint(A, b);
% [x,fval,lambda,iterations,opt_found] = ActiveSetQP(H,g,A,b,x0);
ActiveSetQP(H,g,A,b,x0);

toc
