% function that finds a feasable point in order to find optimal QP solution with active set method

function x0 = gotofeasablepoint(A,b)
    % Find an initial feasible point for the problem Ax <= b using Phase I method
    [m, n] = size(A);

    % Formulate the Phase I LP problem
    f_phase1 = [zeros(n, 1); 1]; % Objective function for Phase I: Minimize t
    A_phase1 = [A, -ones(m, 1)]; % Add the artificial variable t
    lb_phase1 = [-inf(n, 1); 0]; % Lower bound: t >= 0

    % Solve the Phase I LP problem
    options = optimoptions('linprog', 'Display', 'none');
    [x_phase1, ~, exitflag] = linprog(f_phase1, A_phase1, b, [], [], lb_phase1, [], options);

    % Check feasibility
    if exitflag == 1 && x_phase1(end) <= 1e-6
        x0 = x_phase1(1:end-1); % Initial feasible point
    else
        error('The problem is infeasible.');
    end
end
