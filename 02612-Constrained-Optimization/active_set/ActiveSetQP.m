function [x,fval,lambda,iter,opt_found] = ActiveSetQP(H,g,A,b,x0)
%       Active set QP solver for QP in the form:
%       1/2 * x'H x + g*x
%       s.t. A*x >= b

%     setup
    [n,m] = size(H);
    maxiter = 1000;
    x = x0;
    iter = 0;
    STOP = false;
    tolerance = 1;
%     initial Working set
    W = find(abs(A * x - b) < 0);
%     while (~STOP && iter < maxiter)
    opt_found = false;
    lambda = [];
    while (~STOP && iter < maxiter)
        iter = iter +1;
%         Solving EQQP for search direction p using Matlabs quadprog
        options = optimoptions('quadprog', 'Display', 'off'); %options that make quadprog not write ouput data to the console
        p = quadprog(H, H*x + g, A(W, :), b(W), [], [], [], [], [], options);
%         check if norm(p) == 0
        if (norm(p) < tolerance)
            lambda = zeros(m,1);
%             compute all the langrangians and them select only the ones htat are in the working set
            [~, all_lambda] = linprog([], [], [], A(W, :), b(W));
            lambda(W) = all_lambda;
%             check if the optimal solution has been found
            if(all(lambda) >= 0)
%                 if all lambda are positive(or at least greater than the 0), break the while loop
                opt_found = true;
                STOP = true;
            end
%             otherwise remove the most negative lagrangian multiplier index from the working set
            [~, jW] = min(lambda(W));
            j = W(jW);
            W(W == j) = []; % remove constraint with index j from the working set
        else
%             compute step length alpha
            alpha = 0.01;
            for(i = 1:m)
                if(A(i,:)*p > 0)
                    alpha = min(alpha, (b(i)-A(i,:)*x)/(A(i,:)*p));
                end
            end
%             if alpha <1 take step with newly found alpha
            if(alpha < 1)
                x = x + alpha*p;
    %             change wokring set
                W = find(abs(A*x -b) < 0);
            else % otherwise take step with alpha = 1
                x = x + p;
            end
        end
    end
    fval = 1/2 * x' * H * x + g' * x;
    disp("Value of function")
    disp(fval)
end
