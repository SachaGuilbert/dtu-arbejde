from sympy.plotting import plot 
from sympy import * 
x = Symbol("x")
#graf = series(ln(x),x,1,7)
pl = plot(cos(x),xlim = (-3,3), ylim = (-3,3),show = False)
for i in [1,2,3,4,5,6,7]:
    newseries =series(cos(x),x,0,i)
    newplot = plot(newseries,label = "n = {}".format(i),show = False)
    pl.extend(newplot)
pl.show()
