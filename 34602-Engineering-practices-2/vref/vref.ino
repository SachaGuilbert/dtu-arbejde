#include "/home/sacha/DTU/34602-Engineering-practices-2/vref/generatejoke.h"
//Vref pin: 5, input pins for optical sensors: 9 and 10. Overcurrent pin A5
float entry;
bool inputStart = false;
//Vref
const int voltpin = 5;
//Correction
float err;
float prevErr;
float sp = 0;
float pv = 0;

float Kp = 130; //REV up da bugatti
float Ki = 0;
float Kd = 1;

long int Kdtimer = 0;
float indreKi = 0;
float errInt;
float errDiff;
float velocity;
//overcurrent
int overcurrentPin = A5;
bool stuck= false;
//light sensor
int inPin1 = 9;
int inPin2 = 10;
bool value1;
bool value2;
bool prev1;
bool prev2;
bool both = false;
float direction;
long int timer = millis();
float analogOutVref =255/2;
float stopFrekvens = 120;
float errRoof = 1;

void setup() {
    pinMode(voltpin, OUTPUT);
    pinMode(inPin1, INPUT);
    pinMode(inPin2, INPUT);
    Serial.begin(9600);
    Serial.setTimeout(50);
}

void loop() {
  Serial.println("Input startpunkt");
  while(inputStart==false){
    if(Serial.available()>0){
        String input = Serial.readString();
        entry = input.toFloat();
        //Serial.println(entry);
        if(entry >=0 && entry<=90){
            sp = entry;
            pv = entry;
            Serial.print("Start input modtaget: ");
            inputStart = true;
        }
    }
    analogWrite(voltpin,stopFrekvens);
  }
  while(stuck == false){
      //Read position input from serial
        // && err<1 && err>-1
        if(Serial.available()>0){
            String input = Serial.readString();
            entry = input.toFloat();
            //Serial.println(entry);
            if(entry >=0 && entry<=90){
                if(entry<pv){
                    sp = entry;
                }
                if(entry>pv){
                    sp = entry;
                }
                Serial.print("input modtaget: ");
                Serial.println(entry);
                float entrydiff = entry-pv;
                if(entrydiff<5 && entrydiff>2){
                    sp = entry;
                    Kp = 400;
                }else if(entrydiff<2 && entrydiff>0){
                    sp = entry+1;
                    Kp = 300;
                }else if(entrydiff<0 && entrydiff>-5){
                    sp = entry;
                    Kp = 300;
                }
            }
        }
      //Optical sensor for odometry
        prev1 = value1;
        prev2 = value2;
        value1 = digitalRead(inPin1);
        value2 = digitalRead(inPin2);
        if(value2 == HIGH  && prev2 == HIGH && prev1==LOW && value1 ==HIGH){
            direction=1;
        }
        if(value1 == HIGH && prev1 == HIGH && prev2==LOW && value2 ==HIGH){
            direction=-1;
        }
        if(value1==HIGH && value2==HIGH){
            both = true;
        }
      //Opdate position with direction
        if(both==true && value1 == false && value2 == false){
            pv = pv + direction/7.19;
            both = false;
        }
      //Correct error
        if(err<2 && err>-2){
            errRoof = 0.1;
        }else{
            errRoof = 0.3;
        }
        if(millis()>timer+100){
            prevErr = err;
        }
        err = sp-pv;
        errInt = errInt+err;
        errDiff = err-prevErr;
        velocity = (err*Kp + errInt*Ki + errDiff*Kd)*errRoof;

        /*if(err >-3 && err < 3){
            Ki = indreKi;
        }else{
            Ki = 0.01;
        }
        if(err >= -0.7 && err <= 0.7){
            errInt = 0;
        }*/

        analogOutVref = map(velocity, -100, 100, 0, 200);
        if(analogOutVref>255){
            analogOutVref = 255;
        }else if(analogOutVref<0){
            analogOutVref = 0;
        }
        if(pv<45 && analogOutVref>255){
            analogOutVref=200;
        }
        if(millis()>timer+250){
            //Serial.println("Debug:");
            //Serial.print("Position: ");
            Serial.println(pv);
            //Serial.print("vel: ");
            //Serial.println(velocity);
            //Serial.print("volt: ");
            //Serial.println(analogOutVref);
            //Serial.print("val1: ");
            //Serial.println(errRoof);
            //Serial.print("val2: ");
            //Serial.println(value2);
            //Serial.print("both: ");
            //Serial.println(both);
            timer = millis();
        }
        float overcurrent = analogRead(overcurrentPin);
        if(overcurrent>800){
            //Serial.print("current: ");
            //Serial.println(overcurrent);
            stuck = true;
            //Serial.println("Fjern fingeren");
            sp = pv;
            analogOutVref = stopFrekvens;
        }
        analogWrite(voltpin,analogOutVref);
  }
  while(stuck == true){
      if(Serial.available()>0){
            String input = Serial.readString();
            input.trim();
            if(input == "Restart" && analogRead(overcurrentPin)<800){
                //Serial.println("Program restarted");
                stuck = false;
            }
            if(input == "Tell me a joke"){
                printRandomJoke();
                delay(2000);
            }
      }

  }
  inputStart=false;
  
}
