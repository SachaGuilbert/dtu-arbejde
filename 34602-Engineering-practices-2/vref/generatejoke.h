const char *jokes[15] = {
      "Why was the math book sad? Because it had too many problems.",
      "How do you catch a squirrel? Climb up in a tree and act like a nut!",
      "Why don't scientists trust atoms? Because they make up everything.",
      "Why don't eggs tell jokes? Because they'd crack up.",
      "What do you call a bear with no teeth? A gummy bear.",
      "What do you call a snake that works for the government? A civil serpent.",
      "Why did the tomato turn red? Because it saw the salad dressing.",
      "How do you organize a space party? You planet.",
      "What do you call a can opener that doesn't work? A can't opener.",
      "Why did the cookie go to the doctor? Because it was feeling crumbly.",
      "Why did the banana go to the doctor? Because it wasn't peeling well.",
      "What do you call a dinosaur with an extensive vocabulary? A thesaurus.",
      "What do you call a pile of cats? A meowtain.",
      "Why did the computer go to the doctor? It had a byte.",
      "Why did the chicken go to the séance? To talk to the other side."
};
void printRandomJoke(){  
  int randomIndex = random(0, 15);
  Serial.println(jokes[randomIndex]);
}
