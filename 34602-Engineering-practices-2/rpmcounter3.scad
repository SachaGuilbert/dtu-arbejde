$fn=64;
huldia = 3.15;
difference(){
    cylinder(d=50,h=1);
    translate([0,0,-1])cylinder(d=huldia,h=3);
    translate([0,0,-1])cube([40,40,6]);
    translate([-40,-40,-1])cube([40,40,6]);
}

difference(){
    cylinder(d=9,h=3);
    translate([0,0,-1])cylinder(d=huldia,h=6);
}
