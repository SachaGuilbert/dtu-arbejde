from PyQt5 import QtWidgets, QtSerialPort, QtCore
from PyQt5.QtWidgets import QApplication,QMainWindow
import sys
import serial.tools.list_ports

def isfloat(num):
        try:
                float(num)
                return True
        except ValueError:
                return False

class MyWindow(QMainWindow):
        def __init__(self):
                super(MyWindow,self).__init__()
                self.setGeometry(200,200,300,300)
                self.setWindowTitle("Shooting range UI")
                self.initUI()
                self.serialInst = QtSerialPort.QSerialPort('/dev/ttyUSB0',baudRate=QtSerialPort.QSerialPort.Baud9600,readyRead=self.receive)
                self.serialInst.open(QtCore.QIODevice.ReadWrite)

        def initUI(self):
                self.targetposition = 0;
                self.currentposition = 0;

                self.label = QtWidgets.QLabel(self)
                self.label.setText("")
                self.label.move(0,80)

                self.retningLabel = QtWidgets.QLabel(self)
                self.retningLabel.setText("Motor Direction:")
                self.retningLabel.move(0,140)

                self.onsketretning = QtWidgets.QLabel(self)
                self.onsketretning.setText("")
                self.onsketretning.move(120,140)

                self.b1 = QtWidgets.QPushButton(self)
                self.b1.setText("Send input pos")
                self.b1.clicked.connect(self.clicked)
                self.b1.move(0,30)

                self.spinbox = QtWidgets.QDoubleSpinBox(self)
                self.spinbox.move(0,0)
                

                # self.serialInst.readyRead.connect(self.displayData)

        def clicked(self):
                self.targetposition = float(self.spinbox.cleanText())
                self.label.setText(str(self.targetposition))
                self.serialInst.write(str(self.targetposition).encode('Ascii'))
                # print(self.targetposition)
                # print(self.currentposition)
                if self.targetposition > self.currentposition:
                        self.onsketretning.setText("right")
                else:
                        self.onsketretning.setText("left")
                # self.label.setText(self.serialInst.readline())

        def receive(self):
                currentpositionStr = self.serialInst.readLine().data().decode()
                if isfloat(currentpositionStr):
                        self.currentposition = float(currentpositionStr)
                self.label.setText(currentpositionStr)

        

def window():
        app = QApplication(sys.argv)
        win = MyWindow()

        win.show()
        sys.exit(app.exec_())

# while True:
#         if serialInst.in_waiting:
#                 packet = serialInst.readline()
#                 print(packet.decode('utf'))
window()
