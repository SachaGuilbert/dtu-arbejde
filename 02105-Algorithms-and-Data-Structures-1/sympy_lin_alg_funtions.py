from sympy import *
# from numpy.linalg import matrix_rank

# A = Matrix([[3,-1,1,-1,2],[1,-1,-1,-2,-1],[5,-3,1,-3,3],[2,-1,0,-2,1],[1,2,3,4,5]])

# Row echcelon form
# pprint(A_trans.echelon_form())

# Reduced row echeclon form:
# A_rref = A.rref()
# print("The Row echelon form of matrix M and the pivot columns : {}".format(A_rref))

# Inverting a matrix:
# B = Matrix([[1,0,0],[-2,2,0],[1,-2,1]])
# B_inv = B.inv()
# print("Original matrix:",B)
# print("Inverted matrix:",B_inv)
# I_matrix = B*B_inv
# print("Identity matrix I:",I_matrix)

# Transponeret matrix
# A = Matrix([[1,2,-1,2,3],
#             [3,2,1,5,5],
#             [3,6,-3,0,24],
#             [-1,-4,4,-7,11]])
# A_trans = A.transpose()
# print("The rank is:",A_trans.rank())
# pprint(A_trans.echelon_form())

# Kernen til et vektorrum
# A = Matrix([[1,I],
            # [I,1]])
# KerA = A.nullspace()
# b = [A * vector for vector in KerA]
# pprint(KerA)
# pprint(b)
# print((A.transpose()).rank())

# Basisskrifte matrix
# IsA = Matrix([[1,1],
#               [0,1]])
# IsB = Matrix([[1,0],
#               [-1,2]])
# IBs = IsB.inv()
# pprint(IBs)
# IBA = IBs*IsA
# pprint(IBA.inv())

# Determinant af en matrix
# # B = Matrix([[1,0,0],[-2,2,0],[2,-2,1]])
# pprint(B.rank())

x, a, b, c = symbols("x a b c")
# print(expand((1-x)**3))
# print(expand(3*(1-x)**2*x))
# print(expand(3*(1-x)*x**2))
# print(expand(x**3))

IB2S2 = Matrix([[1,0,0],[1,1/2,0],[1,1,1]])
IS2S3 = Matrix([[1,0,0],[0,1,0],[0,0,1],[0,0,0]])
IS3S2 = Matrix([[1,0,0,0],[0,1,0,0],[0,0,1,0]])
IS3B3 = Matrix([[1,0,0,0],[1,1/3,0,0],[1,2/3,1/3,0],[1,1,1,1]])
vec = Matrix([a,b,c])
# pprint(IB2S2*IS3S2*IS3B3)
# pprint(IS2S3*IS2S3.transpose())
pprint(IS2S3.transpose())



# SOLO-BOLO
