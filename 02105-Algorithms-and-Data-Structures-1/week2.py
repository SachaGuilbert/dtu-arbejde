from sympy import *
# from numpy.linalg import matrix_rank

A = Matrix([[3,-1,1,-1,2],[1,-1,-1,-2,-1],[5,-2,1,-3,3],[2,-1,0,-2,1]])

print("Matrix : {} ".format(A))

A_rref = A.rref()
print("The Row echelon form of matrix M and the pivot columns : {}".format(A_rref))
